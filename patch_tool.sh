#!/bin/bash

# Patch Script for Laravel Modules
# Autor: Juan Carlos Nava
# Email: jcnavamorffe@gmail.com

# Script que corrige el fallo de la incompatibilidad de
# Laravel Modules con la nueva version del paquete Symfony console in 5.4.35
# usado en muchos paquetes de paquetes en vendor.

# Referencias:
#
#    https://github.com/nWidart/laravel-modules/issues/1728
#    https://github.com/nWidart/laravel-modules/issues/1727

# Uso

#    chmod +x patch_tool.sh
#    ./patch_tool.sh ruta/de/la/carpeta/a/buscar/de/forma/recursiva


# Función para aplicar el parche a un archivo
aplicar_parche() {
    local archivo="$1"
    sed -i "s/\['feature', false, InputOption::VALUE_NONE, 'Create a feature test.'\]/\['feature', null, InputOption::VALUE_NONE, 'Create a feature test.'\]/" "$archivo"
    echo "Patch aplicado a: $archivo"
}

# Verificar que se haya proporcionado un directorio como argumento
if [ $# -eq 0 ]; then
    echo "Uso: $0 <directorio>"
    exit 1
fi

# Directorio que contiene los archivos a modificar (ruta relativa)
directorio="$1"

# Buscar archivos recursivamente en el directorio
archivos=$(find "$directorio" -type f -name "TestMakeCommand.php")

# Verificar si hay archivos que coincidan con el nombre
if [ -z "$archivos" ]; then
    echo "No se encontraron archivos 'TestMakeCommand.php' en el directorio: $directorio"
    exit 1
fi

# Mostrar los archivos encontrados
echo "Archivos encontrados:"
echo "$archivos"

# Esperar confirmación para proceder
read -p "Presiona Enter para aplicar el parche a los archivos encontrados..."

# Iterar sobre los archivos encontrados
for archivo in $archivos; do
    # Obtener el nombre del directorio del archivo actual
    nombre_directorio=$(basename "$(dirname "$archivo")")

    # Verificar si el nombre del directorio es 'Commands'
    if [ "$nombre_directorio" = "Commands" ]; then
        # Aplicar el parche solo si el directorio es 'Commands'
        aplicar_parche "$archivo"
    else
        echo "Archivo 'TestMakeCommand.php' encontrado en un directorio no esperado: $(dirname "$archivo")"
    fi
done
