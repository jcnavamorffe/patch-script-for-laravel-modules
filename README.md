# Patch Script for Laravel Modules


Script que corrige el fallo de la incompatibilidad de
Laravel Modules con la nueva version del paquete Symfony console in 5.4.35
usado en muchos paquetes de paquetes en vendor.

#### Referencias:
- https://github.com/nWidart/laravel-modules/issues/1728
- https://github.com/nWidart/laravel-modules/issues/1727

#### Uso

    chmod +x patch_tool.sh
    ./patch_tool.sh ruta/de/la/carpeta/a/buscar/de/forma/recursiva
